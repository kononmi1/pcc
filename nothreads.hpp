#ifndef NOTHREADS_H
#define NOTHREADS_H
class Nothreads {
public:
    Nothreads(char file_string[], char file_substring[]);
    void compute_d(int d[]);
    int find_substring(int d[]);
    std::chrono::high_resolution_clock::time_point getCurrentTime();
    void printTimeAndAnswer(std::chrono::high_resolution_clock::time_point start_time, std::chrono::high_resolution_clock::time_point end_time);
    int calculate_size();
    int* change_d_position(int d[]);
    int algorythm();
    const char* getString() const {
        return string;
    }

    void setString(char* file_string) {
        readstring(file_string, string, 1000000);
    }

    const char* getSubstring() const {
        return substring;
    }

    void setSubstring(char* file_substring) {
        readstring(file_substring, substring, 20);
    }

    int getShift() const {
        return shift;
    }

    void setShift(int newShift) {
        shift = newShift;
    }
    int getlen_s() const {
        return len_s;
    }

    void setlen_s(int len_n) {
        len_s = len_n;
    }
    const char* getname_string() const {
        return name_string;
    }

    void setname_string(char* name_string_new) {
        std::strncpy(name_string, name_string_new, sizeof(name_string));
    }

    const char* getname_substring() const {
        return name_substring;
    }

    void setname_substring(char* name_substring_new) {
        std::strncpy(name_substring, name_substring_new, sizeof(name_substring));
    }
    char* readstring(const char* filename, char* outputString, int count) {
        std::ifstream file(filename);
        if (!file) {
            throw std::runtime_error("Failed to open file");
        }
        for (int i = 0; i < count; i++) {
            file >> outputString[i];
        }
        file.close();
        return outputString;
    }

private:
    char string[1000000] = {0}; 
    char substring[20] = {0};
    int shift = 0;
    int len_s = 0;
    char name_string[100] = {0};
    char name_substring[100] = {0};
};
#endif
