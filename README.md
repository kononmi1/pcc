# PCC

## To run Nothreads file
```
Optional
g++ -c main_nothread.cpp -o main_nothreads.o
g++ -c nothreads.cpp -o nothreads.o
Compulsory
g++ main_nothreads.o nothreads.o -o myprogram_nothreads
./myprogram_nothreads substring.txt tests/end/10000_end.txt
```

## To run Threads file
```
Optional
g++ -c main_threads.cpp -o main_threads.o
g++ -c threads.cpp -o threads.o
Compulsory
g++ main_threads.o threads.o -o myprogram_threads
g++ main_threads.o threads.o -o myprogram_threads -pthread
./myprogram_threads substring.txt tests/end/10000_end.txt
```

## To run help command
```
myprogram_nothreads --help
myprogram_threads --help
```

## To run cmake tests
```
cmake .
cmake --build .
ctest -V
```
## For Windows
```
cmake . -G "MinGW Makefiles" -DCMAKE_C_COMPILER="C:/mingw/bin/gcc.exe" -DCMAKE_CXX_COMPILER="C:/mingw/bin/g++.exe"
cmake --build .
ctest -V
```