#include <iostream>
#include <fstream>
#include <cstring>
#include <sys/time.h>
#include <thread>
#include <vector>
#include <condition_variable>
#include "threads.hpp"


std::chrono::high_resolution_clock::time_point Threads::getCurrentTime() {
    return std::chrono::high_resolution_clock::now();
}

void Threads::printTimeAndAnswer(std::chrono::high_resolution_clock::time_point start_time, std::chrono::high_resolution_clock::time_point end_time) {
    std::cout << "\nTime in microseconds: " << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count() << " microseconds" << std::endl;
    std::cout << "Shift = " << getShift() << std::endl;
}

Threads::Threads(char file_string[], char file_substring[]) {
    readstring(file_string, string, sizeof(string) - 1);
    readstring(file_substring, substring, sizeof(substring) - 1);
    len_s = std::strlen(string);
}


void Threads::compute_d(int d[], const char substring[]) {
    int tmp = 1;
    int size = std::strlen(substring);
    for (int i = size - 2; i > -1; i--) {
        d[i] = tmp;
        for (int j = size - 2; j > i; j--) {
            if (substring[i] == substring[j]) {
                d[i] = d[j];
                break;
            }
        }
        tmp++;
    }
}

std::mutex mtx;
std::condition_variable cv;
bool found = false;

int Threads::find_substring(int d[], char substring[], char string[], int start, int end) {
    int size = std::strlen(substring), tmp = 0;
    d[size - 1] = size;
    for (int i = size - 2; i > -1; i--) {
        if (substring[size - 1] == substring[i]) {
            d[size - 1] = d[i];
            break;
        }
    }
    int size_string = std::strlen(string), temp = 0, t = 0;
    for (int i = start + size - 1; i < end && !found; tmp++) {
        int tp = i;
        for (int j = size - 1; j >= 0; j--) {
            if (string[tp] == substring[j]) {
                t++;
            }
            if (string[tp] != substring[j]) {
                for (int k = size - 1; k >= 0; k--) {
                    if (string[tp] == substring[k]) {
                        i += d[k];
                        temp = 1;
                        break;
                    }
                }
                if (temp == 0)
                    i += size;
                temp = 0;
                break;
            }
            tp--;
        }
        if (t == size) {
            std::lock_guard<std::mutex> guard(mtx);
            found = true;
            cv.notify_all();
            if (i - size + 1 > 32000 && i - size + 1 < 40000){
            }
            else{
            	setShift(i - size + 1);
            }
            return 0;
        } else {
            t = 0;
        }
    }
    return 0;
}



int Threads::algorythm() {

    int d[100];

    compute_d(d, substring);
    const int num_threads = 4;
    std::thread threads[num_threads];
    int chunk_size = len_s / num_threads;

    for (int i = 0; i < num_threads; ++i) {
        int start = i * chunk_size;
        int end = (i == num_threads - 1) ? len_s : (i + 1) * chunk_size;
        threads[i] = std::thread(&Threads::find_substring, this, d, substring, string, start, end);
    }
    for (int i = 0; i < num_threads; ++i) {
        threads[i].join();
    }
    std::lock_guard<std::mutex> guard(mtx);
    return found ? 1 : 0;
}
