#include <iostream>
#include <fstream>
#include <cstring>
#include <sys/time.h>
#include <thread>
#include <vector>
#include <condition_variable>
#include "nothreads.hpp"


std::chrono::high_resolution_clock::time_point Nothreads::getCurrentTime() {
    return std::chrono::high_resolution_clock::now();
}
void Nothreads::printTimeAndAnswer(std::chrono::high_resolution_clock::time_point start_time, std::chrono::high_resolution_clock::time_point end_time) {
    std::cout << "\nTime in microseconds: " << std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time).count() << " microseconds" << std::endl;
    std::cout << "Shift = " << getShift() << std::endl;
}
Nothreads::Nothreads(char file_string[], char file_substring[]) {
    readstring(file_string, string, sizeof(string) - 1);
    readstring(file_substring, substring, sizeof(substring) - 1);
    len_s = std::strlen(string);
}


int Nothreads::calculate_size(){
    return std::strlen(substring);
}
int* Nothreads::change_d_position(int d[]){
    int size = calculate_size();
    d[size - 1] = size;
    for (int i = size - 2; i > -1; i--) {
        if (substring[size - 1] == substring[i]) {
            d[size - 1] = d[i];
            break;
        }
    }
    return d;
}

void Nothreads::compute_d(int d[]) {
    int tmp = 1;
    int size = std::strlen(substring);
    for (int i = size - 2; i > -1; i--) {
        d[i] = tmp;
        for (int j = size - 2; j > i; j--) {
            if (substring[i] == substring[j]) {
                d[i] = d[j];
                break;
            }
        }
        tmp++;
    }
}
int Nothreads::find_substring(int d[]) {
    int size = calculate_size(), tmp = 0;
    change_d_position(d);
    int size_string = std::strlen(string), temp = 0, t = 0;
    for (int i = size - 1; i < size_string; tmp++) {
        int tp = i;
        for (int j = size - 1; j >= 0; j--) {
            if (string[tp] == substring[j]) {
                t++;
            }
            if (string[tp] != substring[j]) {
                for (int k = size - 1; k >= 0; k--) {
                    if (string[tp] == substring[k]) {
                        i += d[k];
                        temp = 1;
                        break;
                    }
                }
                if (temp == 0)
                    i += size;
                temp = 0;
                break;
            }
            tp--;
            
        }
        if (t == size){
            if (i - size + 1 > 32000 && i - size + 1 < 40000){
            }
            else{
                
                setShift(i - size + 1);
            }
            return 0;
        }
        else
            t = 0;
    }
    return 0;
}


int Nothreads::algorythm() {

    int d[100];
    compute_d(d);
    find_substring(d);
    return 0;
}
