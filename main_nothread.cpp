#include <iostream>
#include <fstream>
#include <cstring>
#include <sys/time.h>
#include <thread>
#include <vector>
#include <condition_variable>
#include "nothreads.hpp"

int main(int argc, char* argv[]) {
    if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        //g++ -std=c++11 main_nothreads.cpp nothreads.cpp -o myprogram_nothreads
        std::cout << "Hi, there is Nothread main file\nto start program write commands 'cmake .'\nthen 'cmake --build .'\n'make run_tests'" << std::endl;
        return 0;
    }

    Nothreads thr(argv[2], argv[1]);
    auto start_time = thr.getCurrentTime();
    thr.algorythm();
    auto end_time = thr.getCurrentTime();
    thr.printTimeAndAnswer(start_time, end_time);

    return 0;
}
